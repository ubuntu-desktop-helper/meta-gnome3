Source: meta-gnome3
Section: metapackages
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome
Vcs-Git: https://salsa.debian.org/gnome-team/meta-gnome3.git
Vcs-Browser: https://salsa.debian.org/gnome-team/meta-gnome3

# https://gitlab.gnome.org/GNOME/gnome-build-meta/-/tree/master/elements/core
# meta-gnome-core-shell.bst & meta-gnome-core-utilities.bst
Package: gnome-core
Architecture: linux-any
Depends: adwaita-icon-theme,
         at-spi2-core (>= 2.38),
         baobab (>= 3.38),
         dconf-cli (>= 0.38),
         dconf-gsettings-backend (>= 0.38),
         eog (>= 3.38),
         evince (>= 3.38),
         evolution-data-server (>= 3.36),
         fonts-cantarell (>= 0.111),
         gdm3 (>= 3.38) [linux-any],
         gkbd-capplet (>= 3.26),
         glib-networking (>= 2.58),
         gnome-backgrounds (>= 3.38),
         gnome-bluetooth-sendto (>= 42) [linux-any],
         gnome-calculator (>= 3.38),
         gnome-characters (>= 3.34),
         gnome-contacts (>= 3.38),
         gnome-control-center (>= 1:3.38),
         gnome-disk-utility (>= 3.38) [linux-any],
         gnome-font-viewer (>= 3.34),
         gnome-keyring (>= 3.36),
         gnome-logs (>= 3.36) [linux-any],
         gnome-menus (>= 3.36),
         gnome-online-accounts (>= 3.36),
         gnome-session (>= 3.38),
         gnome-settings-daemon (>= 3.38),
         gnome-shell (>= 42) [linux-any],
         gnome-shell-extensions (>= 42) [linux-any],
         gnome-software (>= 3.36),
         gnome-sushi (>= 3.34),
         gnome-system-monitor (>= 3.38),
         gnome-terminal (>= 3.44) | gnome-console (>= 42~beta),
         gnome-text-editor,
         gnome-themes-extra (>= 3.28),
         gnome-user-docs (>= 3.38),
         gnome-user-share (>= 3.34),
         gsettings-desktop-schemas (>= 3.38),
         gstreamer1.0-packagekit,
         gstreamer1.0-plugins-base (>= 1.14),
         gstreamer1.0-plugins-good (>= 1.18),
         gvfs-backends (>= 1.46),
         gvfs-fuse (>= 1.46) [linux-any],
         libatk-adaptor (>= 2.38),
         libcanberra-pulse,
         libglib2.0-bin,
         libpam-gnome-keyring (>= 3.36),
         libproxy1-plugin-gsettings,
         libproxy1-plugin-webkit,
         librsvg2-common [amd64 arm64 armel armhf i386 mips64el mipsel ppc64el s390x powerpc ppc64 riscv64 sparc64],
         nautilus (>= 3.38),
         pipewire-audio,
         sound-theme-freedesktop,
         system-config-printer-common,
         system-config-printer-udev [linux-any],
         totem (>= 3.38),
         tracker,
         xdg-desktop-portal-gnome (>= 41~),
         yelp (>= 3.38),
         zenity (>= 3.32),
         ${misc:Depends}
Recommends: firefox-esr (>= 78) | firefox (>= 78) | chromium | chromium-browser | epiphany-browser | gnome-www-browser,
            libproxy1-plugin-networkmanager [linux-any],
            network-manager-gnome [linux-any],
            ${lmm:Recommends},
Suggests: gnome
Description: GNOME Desktop Environment -- essential components
 These are the core components of the GNOME Desktop environment, an
 intuitive and attractive desktop.
 .
 This metapackage depends on a basic set of programs, including a file
 manager, an image viewer, a web browser, a video player and other
 tools.
 .
 It contains the official “core” modules of the GNOME desktop.

Package: gnome
Architecture: linux-any
Depends: gnome-core (= ${binary:Version}),
         desktop-base,
         libproxy1-plugin-networkmanager [linux-any],
         network-manager-gnome (>= 1.8) [linux-any],
# Official apps modules
         cheese (>= 3.38) [linux-any],
         file-roller (>= 3.38),
         gnome-calendar (>= 3.38),
         gnome-clocks (>= 3.38),
         gnome-color-manager (>= 3.36),
         gnome-maps (>= 3.38),
         gnome-music (>= 3.36),
         shotwell | gnome-photos (>= 3.36),
         gnome-weather (>= 3.36),
         orca (>= 3.38),
         rygel-playbin (>= 0.36) [!kfreebsd-any],
         rygel-tracker (>= 0.36) [!kfreebsd-any],
         simple-scan (>= 3.36) [linux-any],
# More applications
         avahi-daemon,
         evolution (>= 3.36),
         gnome-sound-recorder,
         gnome-tweaks (>= 3.30),
         libgsf-bin,
         libreoffice-gnome [!riscv64 !mips64el !mipsel],
         libreoffice-writer [!riscv64 !mips64el !mipsel],
         libreoffice-calc [!riscv64 !mips64el !mipsel],
         libreoffice-impress [!riscv64 !mips64el !mipsel],
         rhythmbox (>= 3.0),
         seahorse (>= 3.36),
         xdg-user-dirs-gtk,
# Plugins for core and apps
         cups-pk-helper (>= 0.2),
         evolution-plugins (>= 3.36),
         gstreamer1.0-libav (>= 1.10),
         gstreamer1.0-plugins-ugly (>= 1.10),
         rhythmbox-plugins,
         rhythmbox-plugin-cdrecorder [!hurd-i386],
         totem-plugins,
         ${misc:Depends}
Recommends: gnome-games,
            gnome-initial-setup,
            gnome-remote-desktop,
            transmission-gtk
Suggests: alacarte,
          firefox-esr-l10n-all | firefox-l10n-all,
          goobox | sound-juicer,
          polari,
          vinagre (>= 3.22),
          webext-ublock-origin-firefox | webext-ublock-origin-chromium,
Description: Full GNOME Desktop Environment, with extra components
 This is the GNOME Desktop environment, an intuitive and attractive
 desktop, with extra components.
 .
 This metapackage depends on the standard distribution of the GNOME
 desktop environment, plus a complete range of plugins and other
 applications integrating with GNOME and Debian, providing the best
 possible environment to date.

# This is custom for Debian. Historically, GNOME had a monorepo
# and releases for gnome-games but these were later split to
# separate repos and projects. Some apps like GNOME 2048 were
# never part of the monorepo but it makes sense to add them here.
Package: gnome-games
Architecture: all
Depends: aisleriot (>= 1:3.22),
         five-or-more (>= 1:3.32),
         four-in-a-row (>= 1:3.38),
         gnome-2048 (>= 3.38),
         gnome-chess (>= 1:3.38),
         gnome-klotski (>= 1:3.38),
         gnome-mahjongg (>= 1:3.38),
         gnome-mines (>= 1:3.36),
         gnome-nibbles (>= 1:3.38),
         gnome-robots (>= 1:3.38),
         gnome-sudoku (>= 1:3.38),
         gnome-taquin (>= 3.38),
         gnome-tetravex (>= 1:3.38),
         hitori (>= 3.38),
         iagno (>= 1:3.38),
         lightsoff (>= 1:3.38),
         quadrapassel (>= 1:3.36),
         swell-foop (>= 1:3.34),
         tali (>= 1:3.38),
         ${misc:Depends}
Description: games for the GNOME desktop
 These are the games from the official GNOME release. They have the look and
 feel of the GNOME desktop, but can be used without it.
 .
 Games are now split in separate packages; this metapackage brings all
 of them but they can be installed separately.

# Custom for Debian
Package: gnome-platform-devel
Architecture: all
Section: devel
Depends: gtk-doc-tools (>= 1.21),
         libadwaita-1-dev (>= 1.1),
         libgstreamer1.0-dev (>= 1.10),
         libgtk-4-dev (>= 4.6),
         meson,
         ${misc:Depends}
Recommends: gnome-api-docs
Description: GNOME development platform
 These are the development components that are part of the development
 “platform” for the GNOME Desktop environment, an intuitive and
 attractive desktop.
 .
 This metapackage depends on the recommended components to develop and
 build applications using GNOME technologies. Most of these components
 have long-term API stability guarantees.

# meta-gnome-core-developer-tools.bst
Package: gnome-devel
Architecture: all
Section: devel
Depends: dconf-editor (>= 3.38),
         devhelp (>= 3.38),
         gnome-boxes (>= 3.38),
         gnome-builder (>= 3.38),
         gnome-devel-docs (>= 3.38),
         ${misc:Depends}
Recommends: gnome-platform-devel
# GNOME Builder includes these features using their libraries
Suggests: d-spy,
          sysprof
Description: GNOME Desktop Environment -- development tools
 These are the development tools of the GNOME Desktop environment, an
 intuitive and attractive desktop.
 .
 This metapackage depends on a recommended set of applications to
 develop new programs for GNOME.

# Custom for Debian
Package: gnome-api-docs
Architecture: all
Section: doc
Depends: gnome-devel-docs,
         libadwaita-1-doc,
         libatk1.0-doc,
         libgdk-pixbuf2.0-doc,
         libglib2.0-doc,
         libgtk-4-doc,
         libpango1.0-doc,
         ${misc:Depends}
Recommends: devhelp,
            libcairo2-doc,
            libcanberra-doc,
            libnotify-doc,
            libtelepathy-glib-doc,
            libxml2-doc,
            policykit-1-doc
Suggests: gnome-core-devel,
          gstreamer1.0-doc,
          python3-doc
Description: API reference documentation for the GNOME libraries
 This metapackage brings all available API documentation for the
 libraries of the GNOME platform, as well as their important
 dependencies. This should be most of the documentation you need to
 write GNOME applications.
 .
 This documentation is best viewed within the devhelp documentation
 browser.
